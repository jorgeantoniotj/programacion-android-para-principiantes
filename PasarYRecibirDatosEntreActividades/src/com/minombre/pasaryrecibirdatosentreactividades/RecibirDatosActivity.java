package com.minombre.pasaryrecibirdatosentreactividades;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class RecibirDatosActivity extends Activity{

	private TextView lblNombre;
	private TextView lblCelular;
	private TextView lblCorreo;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_recibir_datos);
		
		this.lblNombre = (TextView)findViewById(R.id.lblNombre);
		this.lblCelular = (TextView) findViewById(R.id.lblCelular);
		this.lblCorreo = (TextView)findViewById(R.id.lblCorreo);
		
		Bundle extras = getIntent().getExtras();
		
		/*
		this.lblNombre.setText("Nombre: "+extras.getString("txtNombre"));
		this.lblCelular.setText("Celular: "+extras.getString("txtCelular"));
		this.lblCorreo.setText("Correo: "+extras.getString("txtCorreo"));*/
		
		Contacto contacto = (Contacto) extras.getSerializable("contacto");
		this.lblNombre.setText("Nombre: "+contacto.getNombre());
		this.lblCelular.setText("Celular: "+contacto.getCelular());
		this.lblCorreo.setText("Correo: "+contacto.getCorreo());
	}
}
