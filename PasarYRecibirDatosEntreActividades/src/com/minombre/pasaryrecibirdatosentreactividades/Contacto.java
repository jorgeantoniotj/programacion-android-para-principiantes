package com.minombre.pasaryrecibirdatosentreactividades;

import java.io.Serializable;

public class Contacto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String nombre;
	private String correo;
	private String celular;
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	
	

}
