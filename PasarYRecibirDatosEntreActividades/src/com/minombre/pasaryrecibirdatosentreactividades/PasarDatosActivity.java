package com.minombre.pasaryrecibirdatosentreactividades;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class PasarDatosActivity extends Activity{

	private EditText txtNombre;
	private EditText txtCelular;
	private EditText txtCorreo;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pasar_datos);
		
		this.txtNombre = (EditText)findViewById(R.id.txtNombre);
		this.txtCelular = (EditText) findViewById(R.id.txtCelular);
		this.txtCorreo = (EditText)findViewById(R.id.txtCorreo);
	}
	
	public void onClick(View view)
	{
		Contacto contacto = new Contacto();
		contacto.setNombre(this.txtNombre.getText().toString());
		contacto.setCelular(this.txtCelular.getText().toString());
		contacto.setCorreo(this.txtCorreo.getText().toString());
		
		Bundle extras = new Bundle();
		/*
		extras.putString("txtNombre", this.txtNombre.getText().toString());
		extras.putString("txtCelular", this.txtCelular.getText().toString());
		extras.putString("txtCorreo", this.txtCorreo.getText().toString());*/
		
		extras.putSerializable("contacto", contacto);
			
		Intent intentRecibirDatos = new Intent(this, RecibirDatosActivity.class);
		intentRecibirDatos.putExtras(extras);
		startActivity(intentRecibirDatos);
		
	}
}
