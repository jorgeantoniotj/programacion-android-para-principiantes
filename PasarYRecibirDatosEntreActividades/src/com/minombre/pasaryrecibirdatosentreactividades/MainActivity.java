package com.minombre.pasaryrecibirdatosentreactividades;

import android.os.Bundle;
import android.view.View;
import android.app.Activity;
import android.content.Intent;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}
	
	public void onClick(View view)
	{
		switch (view.getId()) {
			
			case R.id.btnEjemploPasarDatos:
				
				Intent intentEjemploPasarDatos = new Intent(this, PasarDatosActivity.class);
				startActivity(intentEjemploPasarDatos);
				
				break;
				
			case R.id.btnEjemploRecibirDatos:
				
				break;

			default:
				break;
		}
	}

}
