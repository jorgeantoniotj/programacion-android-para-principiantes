package interfaces;

public interface IMusico {

	public void tocarIntrumento(String instrumento);
	public void dejarDeTocar();
}
