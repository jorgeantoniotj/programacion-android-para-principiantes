package clases;


import java.util.List;

public class Alumno extends Persona{

	private String matricula;
	private List<Materia> listaMaterias;
	
	public Alumno(String nombre, String apPaterno, String apMaterno, int edad, String matricula) {
		super(nombre, apPaterno, apMaterno, edad);
		this.matricula = matricula;
	}
		
	@Override
	public void saludar() {
		System.out.println(String.format("Hola soy %s y soy un Alumno", super.getNombreCompleto()));
	}
	
	@Override
	public String toString() {
		return "Alumno [matricula=" + matricula + ", nombre=" + super.getNombre() + ", apPaterno=" + super.getApPaterno() 
				+ ", apMaterno=" + super.getApMaterno() + ", edad=" + super.getEdad() + ", matricula="+ this.getMatricula()+ "]";
	}
	
	public void imprimirMiCargaAcademica() {
		
		if (this.listaMaterias != null && listaMaterias.size() > 0) {
			System.out.println("\nLas materias que curso son:");
			for (Materia materia : listaMaterias) {
				System.out.println(String.format("Nombre: %s , creditos: %d", materia.getNombre(), materia.getCreditos()));
			}
		}else{
			System.out.println("Por el momento no curso ninguna materia");
		}
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public List<Materia> getListaMaterias() {
		return listaMaterias;
	}

	public void setListaMaterias(List<Materia> listaMaterias) {
		this.listaMaterias = listaMaterias;
	}

	
}
