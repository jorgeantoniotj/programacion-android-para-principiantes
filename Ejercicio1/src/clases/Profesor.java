package clases;


import interfaces.IMusico;

import java.util.ArrayList;

public class Profesor extends Persona implements IMusico{

	//Atributos
	private String gradoAcademico;
	private double sueldo;
	private ArrayList<Materia> listaMaterias;

	//Constructor con parametros
	public Profesor(String nombre, String apPaterno, String apMaterno, int edad, String gradoAcademico, double sueldo) {
		super(nombre, apPaterno, apMaterno, edad);
		this.gradoAcademico = gradoAcademico;
		this.sueldo = sueldo;
	}

	// Encapsular atributos a travez de propiedades (Métodos de acceso get y set)
	public String getGradoAcademico() {
		return gradoAcademico;
	}

	public void setGradoAcademico(String gradoAcademico) {
		this.gradoAcademico = gradoAcademico;
	}

	public double getSueldo() {
		return sueldo;
	}

	public void setSueldo(double sueldo) {
		this.sueldo = sueldo;
	}

	public ArrayList<Materia> getListaMaterias() {
		return listaMaterias;
	}

	public void setListaMaterias(ArrayList<Materia> listaMaterias) {
		this.listaMaterias = listaMaterias;
	}
	
	// Métodos
	@Override
	public void saludar() {
		System.out.println(String.format("Hola soy %s y soy un Profesor", super.getNombreCompleto()));
	}	

	@Override
	public String toString() {
		return "Profesor [nombre=" + super.getNombre() + ", apPaterno=" + super.getApPaterno() 
				+ ", apMaterno=" + super.getApMaterno() + ", edad=" + super.getEdad() +", gradoAcademico= " +this.getGradoAcademico()
				+ ", sueldo=" + this.getSueldo() + "]";
	}
	
	public void mostrarListaMaterias() {
		
		if (this.listaMaterias != null && listaMaterias.size() > 0) {
			System.out.println("\nLas materias que imparto son:");
			for (Materia materia : listaMaterias) {
				System.out.println(String.format("Nombre: %s , creditos: %d", materia.getNombre(), materia.getCreditos()));
			}
		}else{
			System.out.println("Por el momento no tengo asignada ninguna materia");
		}
	}

	@Override
	public void tocarIntrumento(String instrumento) {
		System.out.println(String.format("Estoy tocando mi %s", instrumento));
	}

	@Override
	public void dejarDeTocar() {
		System.out.println("Deje de tocar");
	}


}
	


