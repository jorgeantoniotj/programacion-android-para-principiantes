package com.minombre.mapas;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;


public class MainActivity extends FragmentActivity {

	private final String TAG = getClass().getName();
	private GoogleMap mapa;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		this.mapa = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapa)).getMap();
		this.mapa.setMyLocationEnabled(true);
		//this.mapa.setMapType(GoogleMap.MAP_TYPE_HYBRID);
		//this.mapa.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
		//this.mapa.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
		
		LatLng coordenadas = new LatLng(18.931619, -99.224106);
		
		this.mapa.addMarker(new MarkerOptions().position(coordenadas).title("Cuernavaca"));
		this.mapa.moveCamera(CameraUpdateFactory.newLatLngZoom(coordenadas, 13));
	}

}
