package com.minombre.holamundo;

import android.os.Bundle;
import android.util.Log;
import android.app.Activity;

public class MainActivity extends Activity {

	private final String TAG = getClass().getName();
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");
        setContentView(R.layout.activity_main);
    }
    
    @Override
    protected void onStart() {
    	super.onStart();
    	Log.i(TAG, "onStart");
    }
    
    @Override
    protected void onResume() {
    	super.onResume();
    	Log.i(TAG, "onResume");
    }
    
    @Override
    protected void onPause() {
    	super.onPause();
    	Log.i(TAG, "onPause");
    }
    
    @Override
    protected void onStop() {
    	super.onStop();
    	Log.i(TAG, "onStop");
    }
    
    @Override
    protected void onRestart() {
    	super.onRestart();
    	Log.i(TAG, "onRestart");
    }
    
    @Override
    protected void onDestroy() {
    	super.onDestroy();
    	Log.i(TAG, "onDestroy");
    }
  
}
