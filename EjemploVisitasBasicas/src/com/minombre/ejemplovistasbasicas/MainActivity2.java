package com.minombre.ejemplovistasbasicas;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity2 extends Activity{

	private EditText txtNombre;
	private EditText txtCelular;
	private EditText txtCorreo;
	private TextView lblNombre;
	private TextView lblCelular;
	private TextView lblCorreo;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main2);
		
		this.txtNombre = (EditText)findViewById(R.id.txtNombre);
		this.txtCelular = (EditText) findViewById(R.id.txtCelular);
		this.txtCorreo = (EditText)findViewById(R.id.txtCorreo);
		this.lblNombre = (TextView)findViewById(R.id.lblNombre);
		this.lblCelular = (TextView) findViewById(R.id.lblCelular);
		this.lblCorreo = (TextView)findViewById(R.id.lblCorreo);
	}
	
	public void onClick(View view)
	{
		this.lblNombre.setText("Nombre: "+this.txtNombre.getText().toString());
		this.lblCelular.setText("Celular: "+this.txtCelular.getText().toString());
		this.lblCorreo.setText("Correo: "+this.txtCorreo.getText().toString());
	}
}
