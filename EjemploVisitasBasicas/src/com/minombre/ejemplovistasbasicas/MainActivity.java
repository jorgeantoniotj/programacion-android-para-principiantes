package com.minombre.ejemplovistasbasicas;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.app.Activity;

//Implementamos la interface OnClickListener a nuestra Actividad
//public class MainActivity extends Activity implements OnClickListener{
public class MainActivity extends Activity{
	
	//Declaración de nuestras propiedades
	private EditText  txtNombre;
	//private Button btnEnviar;
	private TextView lblNombre;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		//Inicializamos nuestras propiedades
		this.txtNombre = (EditText) findViewById(R.id.txtNombre);
		//this.btnEnviar = (Button) findViewById(R.id.btnEnviar);
		this.lblNombre = (TextView) findViewById(R.id.lblNombre);
		
		/*
		 * Establecemos el evento OnCLickListener en "btnEnviar"
		 * y en su implementación establecemos a "lblNombre" la cadena "Hola" contatenada 
		 * al texto que contenga "txtNombre"
		 */
		/*
		this.btnEnviar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View view) {
				MainActivity.this.lblNombre.setText("Hola: "+MainActivity.this.txtNombre.getText().toString());
			}
		});*/
		//this.btnEnviar.setOnClickListener(this);
	}
	
	/*Cuando implementamos la interface de esta forma no necesitamos hacer referencia nuestros 
	 * atributos utilizando "MainActivity.this" ahora podemos usar solamente "this"
	 */
	
	//@Override
	public void onClick(View view) {
		this.lblNombre.setText("Hola: "+this.txtNombre.getText().toString());
	}
}
