import java.util.ArrayList;

import clases.Materia;
import clases.Persona;
import clases.Profesor;


public class Principal {

	public static void main(String args[])
	{
		
		//Instanciamos una persona con el constructor vacio el cual iniciara nuestro objeto con los 
		//valores que le asignamos.
		Persona persona1 = new Persona();
		
		//Comprobamos el valor de los atributos, imprimiendo en pantalla el String que nos regresa el método toString
		//que sobreescribimos del objeto padre Object.
		System.out.println(persona1.toString());
		
		//Llamado de método saludar
		persona1.saludar();
	
		//Cambio de atributos a travez de nuestras propiedades (Métodos de acceso get y set)
		persona1.setNombre("Alvaro");
		persona1.setApPaterno("Vargas");
		persona1.setApMaterno("Velasquez");
		persona1.setEdad(23);
		
		//Imprimimos nuevamente la cadena del método toString y llamamos al método saludar
		//para comprobar el cambio de los atributos.
		System.out.println(persona1.toString());
		persona1.saludar();
		
		//Creamos una segunda instancia de Persona pero ahora utilizaremos el constructor con parametros
		Persona persona2 = new Persona("Roberto","Román", "Diaz", 27);
		System.out.println(persona2.toString());
		persona2.saludar();
		
		//Instanciamos un Profesor con el constructor con parametros que creamos
		Profesor profesor = new Profesor("Miguel", "Montes", "Figueroa", 23, "Ing.", 500);
		System.out.println(profesor.toString());
		profesor.saludar();
		
		//Al invocar el método mostrarListaMaterias nos deberia aparecer el mensaje
		// "Por el momento no tengo asignada ninguna materia" debido a que no asignamos ningúna lista de materias.
		profesor.mostrarListaMaterias();
		
		//Si quisieramos hacer una instancia de un profersor con el constructor vacio nos marcaria error 
		//debido a que no definimos este constructor.
		//Profesor profesor2 = new Profesor();
		
		//Creamos la lista de materias 
		ArrayList<Materia> listaMaterias = new ArrayList<Materia>();
		listaMaterias.add(new Materia("Fundamentos de programación", 10));
		listaMaterias.add(new Materia("Fundamentos de base de datos", 8));
		
		//Asignamos la lista de materias al profesor a travez del método set de lista de materias y las mostramos en pantalla
		profesor.setListaMaterias(listaMaterias);
		profesor.mostrarListaMaterias();
		
		profesor.tocarIntrumento("Guitarra");
		profesor.dejarDeTocar();

	}
}
