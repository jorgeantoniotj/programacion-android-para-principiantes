package clases;

public class Persona {

	// Atributos
	private String nombre;
	private String apPaterno;
	private String apMaterno;
	private int edad;

	// Contructor predeterminado
	public Persona() {
		this.nombre = "Juan";
		this.apPaterno = "Perez";
		this.apMaterno = "Sanchez";
		this.edad = 25;
	}

	// Contructor con parametros
	public Persona(String nombre, String apPaterno, String apMaterno, int edad) {
		this.nombre = nombre;
		this.apPaterno = apPaterno;
		this.apMaterno = apMaterno;
		this.edad = edad;
	}

	// Encapsular atributos a travez de propiedades (Métodos de acceso get y
	// set)
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApPaterno() {
		return apPaterno;
	}

	public void setApPaterno(String apPaterno) {
		this.apPaterno = apPaterno;
	}

	public String getApMaterno() {
		return apMaterno;
	}

	public void setApMaterno(String apMaterno) {
		this.apMaterno = apMaterno;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	// Métodos
	protected String getNombreCompleto() {
		return String.format("%s %s %s", this.nombre, this.apPaterno, this.apMaterno);
	}

	public void saludar() {
		System.out.println(String.format("Hola soy %s y soy una Persona", this.getNombreCompleto()));
	}

	// Sobreescritura del método toString
	@Override
	public String toString() {
		return "Persona [nombre=" + nombre + ", apPaterno=" + apPaterno + ", apMaterno=" + apMaterno + ", edad=" + edad + "]";
	}

}
