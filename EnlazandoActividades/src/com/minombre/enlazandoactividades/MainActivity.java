package com.minombre.enlazandoactividades;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.app.Activity;
import android.content.Intent;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}
	
	public void onClick(View view)
	{
		switch (view.getId()) {
			
			case R.id.btnActividad2:
				
				Intent intentActividad2 = new Intent(this, SecondActivity.class);
				startActivity(intentActividad2);
				
				break;
				
			case R.id.btnActividad3:
				
				Intent intentActividad3 = new Intent(this, ThirdActivity.class);
				startActivity(intentActividad3);
				
				break;
				
			case R.id.btnNavegador:
				
				Intent intentNavegador  = new Intent(Intent.ACTION_VIEW,Uri.parse("http://www.google.com")); 	
				startActivity(intentNavegador);
				
				break;
				
			case R.id.btnLlamarContacto:
				
				Intent intentLlamada  = new Intent(Intent.ACTION_CALL,Uri.parse("tel:(+52)7771117362")); 	
				startActivity(intentLlamada);
				
				break;
		}
	}
}
