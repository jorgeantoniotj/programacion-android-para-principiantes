package com.minombre.ejercicio3calculadora;

import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

	private final String TAG = getClass().getName();
	private final String CERO = "0", MULTIPLICACION = "*", SUMA = "+", RESTA = "-", DIVISION = "/";
	private TextView lblOperaciones;
	private EditText txtNumero;
	private boolean mostrarResultado;
	private boolean cambiarOperacion;
	private String ultimaOperacion;
	private Double operando1, operando2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		this.txtNumero = (EditText) findViewById(R.id.txtNumero);
		this.lblOperaciones = (TextView) findViewById(R.id.lblOperaciones);
		this.reiniciarValores();
	}
	
	public void onClick(View view) {
		Button button = (Button) view;
		String numero = txtNumero.getText().toString();
		String digito = button.getText().toString();

		switch (view.getId()) {

			case R.id.btnBorrarDigito:
				this.borrarDigito(numero);
				break;

			case R.id.btnReiniciarNumero:
				this.reiniciarNumero();
				break;

			case R.id.btnReiniciarValores:
				this.reiniciarValores();
				break;

			case R.id.btnIgual:
				if (this.operando1 != null)
					this.realizarOperacion(numero, false);
				break;

			case R.id.btnDivision:
			case R.id.btnMultiplicacion:
			case R.id.btnResta:
			case R.id.btnSuma:
				
				if(this.operando1 == null)
					this.iniciarOperacion(numero,digito);
				else if(this.cambiarOperacion)
					this.cambiarOperacion(digito);
				else 
					this.resolverOperacion(numero, digito);
					
				this.cambiarOperacion = true;
				
				break;

			default:
				this.agregarDigito(digito);
				break;
		}
	}


	public void borrarDigito(String numero) {

		int lenNumero = numero.length();

		if (lenNumero > 1) {
			numero = numero.substring(0, lenNumero - 1);
			this.txtNumero.setText(numero);
		} else {
			reiniciarNumero();
		}
	}


	public void agregarDigito(String digito) {
					
		if (txtNumero.getText().toString().equals(CERO) || mostrarResultado){
			this.mostrarResultado = false;
			this.cambiarOperacion = false;
			this.txtNumero.setText("");
		}

		this.txtNumero.append(digito);
	}


	public void reiniciarNumero() {
		this.txtNumero.setText(CERO);
	}


	public void reiniciarValores() {
		this.lblOperaciones.setText("");
		this.txtNumero.setText(String.valueOf(CERO));
		this.ultimaOperacion = "";
		this.mostrarResultado = false;
		this.cambiarOperacion = false;
		this.operando1 = null;
		this.operando2 = null;
	}

	public void resolverOperacion(String numero, String operacion) {
		
		this.lblOperaciones.append(String.format(" %s %s", numero, operacion));
		this.realizarOperacion(numero, true);
		this.ultimaOperacion = operacion;

	}
	
	private void iniciarOperacion(String numero, String operacion)
	{
		this.lblOperaciones.append(String.format(" %s %s", numero, operacion));
		this.operando1 = Double.parseDouble(numero);
		this.ultimaOperacion = operacion;
		this.reiniciarNumero();
	}
	
	private void cambiarOperacion(String operacion){
		
		String texto = this.lblOperaciones.getText().toString();
		int lenOperaciones = texto.length();
		
		if(lenOperaciones > 0){
			texto = texto.substring(0, lenOperaciones-2);
			this.lblOperaciones.setText(String.format("%s %s", texto, operacion));
			this.ultimaOperacion = operacion;
		}
		
	}

	private void realizarOperacion(String numero,boolean guardarResultado) {

		this.operando2 = Double.parseDouble(numero);
		
		if (this.ultimaOperacion.equals(MULTIPLICACION)) 
			this.operando1 = this.operando1 * this.operando2;
		else if (this.ultimaOperacion.equals(SUMA)) 
			this.operando1 = this.operando1 + this.operando2;
		else if (this.ultimaOperacion.equals(RESTA))
			this.operando1 = this.operando1 - this.operando2;
		else if (this.ultimaOperacion.equals(DIVISION))
			this.operando1 = this.operando1 / this.operando2;

		this.txtNumero.setText(this.operando1.toString());
		this.mostrarResultado = true;
		
		if(!guardarResultado){
			this.lblOperaciones.setText("");
			this.ultimaOperacion = "";
			this.operando1 = null;
			this.operando2 = null;
		}
	}

}
